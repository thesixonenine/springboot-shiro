package cn.wolfcode.shiro.security;

import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author simple
 * @version 1.0
 * @date 2018/9/30 11:45
 */
@Configuration
public class ShiroConfig {
    /**
     * 创建安全管理器对象,关联自定义Realm
     *
     * @param userRealm userRealm
     * @return DefaultWebSecurityManager
     */
    @Bean
    public DefaultWebSecurityManager securityManager(UserRealm userRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm);
        return securityManager;
    }

    /**
     * 定义拦截的规则
     *
     * @return ShiroFilterChainDefinition
     */
    @Bean
    public ShiroFilterChainDefinition shiroFilterChainDefinition() {
        DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();
        //浏览器标签页的图标放行.
        chainDefinition.addPathDefinition("/**/favicon.ico", "anon");
        //静态资源放行
        chainDefinition.addPathDefinition("/js/**", "anon");
        chainDefinition.addPathDefinition("/html/**", "anon");
        chainDefinition.addPathDefinition("/css/**", "anon");
        chainDefinition.addPathDefinition("/test", "anon");
        //剩下的资源都需要登录才能访问
        chainDefinition.addPathDefinition("/**", "authc");
        return chainDefinition;
    }
}
