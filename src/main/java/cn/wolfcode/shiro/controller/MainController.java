package cn.wolfcode.shiro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author simple
 */
@Controller
public class MainController {
    @RequestMapping("/main")
    public String mainPage() {
        return "main";
    }

    @RequestMapping("/test")
    @ResponseBody
    public String test() {
        return "Hello World";
    }
}
